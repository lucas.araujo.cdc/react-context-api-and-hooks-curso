export const Button = ({children, onButtonClick, disabled=false}) =>{

  return (
    <button  style={{fontSize: '60px'}} onClick={onButtonClick } disabled={disabled}>
      {children}
    </button>
  )

}

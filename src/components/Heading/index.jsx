import { useState } from "react"
import { useCounterContext } from "../../contexts/CounterContext"


export const Heading = ({children})=>{
  const [state, action] = useCounterContext()
  return <h1 style={{fontSize: "60px"}}>{state.counter}</h1>
}
